#!/usr/bin/python
# -*- coding: utf-8 -*-


import json
import string


def createSurveyName(json_list):
	for j in json_list:
		if "brand_name" in j["question"] and j["answer"] !=0:
			return str(j["answer"]) + "_main"


def createAnswer (some_key, json_list):
	answer_json = []
	i = 1
	for j in json_list:
		if some_key in j["question"] and j["answer"] !=0:
			answer_json.append({"text": j["answer"], "position":i})
			i+=1
	return answer_json

def createAnswerQ24 (some_key, json_list):
	answer_json = []
	i = 1
	for j in json_list:
		if some_key in j["question"] and j["answer"] !=0:
			answer_json.append({"text": j["answer"], "position":i})
			i+=1
	answer_json.append({"text": "Никакой", "position":i})
	return answer_json	



def createHeading (some_key, json_list):
	answers = []
	answer_json = []
	for j in json_list:
		if some_key in j["question"] and j["answer"] !=0:
			answers.append(j["answer"])
	s = "<br>".join(str(x) for x in answers)
	answer_json.append({"heading": s})
	return answer_json	


def createSeveralQ20 (json_list):
	question_j = []
	i = 0
	k = 1
	for j in json_list:
		if "sku_type" in j["question"] and j["answer"] !=0:
			create_headings = {"heading": "{{" + "custom.q20" + string.ascii_lowercase[i]  + "}}" + j["answer"]}
			question_j.append({
					"family": "open_ended",
					"subtype": "multi",
					"required": {
						"text": "Необходимо ответить на вопрос.",
						"amount": "4",
						"type": "all"
					},								
					"answers": {
						"rows": [{
								"text": "При какой цене Вы купите этот продукт и решите, что сделали выгодную покупку?",
								"position": 1
							},
							{
								"text": "При какой цене Вы посчитаете, что этот продукт стоит дороже, чем следует, но все же купите?",
								"position": 2
							},
							{
								"text": "Начиная с какой цены, Вы решите, что этот продукт стоит слишком дорого, и не станете его покупать?",
								"position": 3
							},
							{
								"text": "Начиная с какой цены, Вы решите, что цена вызывает сомнения в качестве этого продукта, и не станете его покупать?",
								"position": 4
							}
						]
					},
					"position": k,
					"validation": {
						"text": "Введите число. Дробные, процентные значения и нецифровые символы не допускаются.",
						"type": "integer",
						"min": 0,
						"max": 10000
					},
					"headings": [create_headings]
					})
			k+=1
			i+=1
	s = ",".join(str(x) for x in question_j)
	return s

def createSeveralQ32 (json_list):
	question_j = []
	i = 0
	k = 1
	for j in json_list:
		if "sku_type" in j["question"] and j["answer"] !=0:
			create_headings = {"heading": "{{" + "custom.q32" + string.ascii_lowercase[i]  + "}}" + j["answer"]}
			question_j.append({
					"family": "single_choice",
					"subtype": "vertical",
					"required": {
						"text": "Необходимо ответить на вопрос.",
						"amount": "1",
						"type": "all"
					},
					"answers": {
						"choices": [{
								"text": "Раз в день и чаще",
								"position": 1
							},
							{
								"text": "4-6 раз в неделю",
								"position": 2
							},
							{
								"text": "2-3 раза в неделю",
								"position": 3
							},
							{
								"text": "Раз в неделю",
								"position": 4
							},
							{
								"text": "Раз в 2 недели",
								"position": 5
							},
							{
								"text": "Раз в месяц",
								"position": 6
							},
							{
								"text": "Реже, чем раз в месяц",
								"position": 7
							},
							{
								"text": "Никогда",
								"position": 8
							}
						],
						"other": [{
								"text": "<br />Буду покупать количество упаковок за 1 раз:<br /><br />(Если Вы не стали бы покупать этот новый продукт в каком-либо формате, отмечайте 0 для количества упаковок определенного формата)<br /><br />Введите количество упаковок",
								"num_chars": 10,
								"num_lines": 1
							}]
					},
					
					"headings": [create_headings],
					"position": k,
					"validation": {
						"min": 0,
						"text": "Комментарий введен в неправильном формате.",
						"max": 2000,
						"type": "integer"}
				})
			k+=1
			i+=1
	s = ",".join(str(x) for x in question_j)
	return s


def createQ38(json_list):
	j_answer = []
	for j in json_list:
		if "concept_part_1" in j["question"] and j["answer"] !=0:
			j_answer.append({"heading": "{{" + "q38" + "}}" + "Насколько проблема, которую Вы узнали из описания об этом новом продукте, является важной лично для Вас?<p>" + j["answer"] + "</p>"})
	return j_answer	


def createQ39(json_list):
	j_answer = []
	for j in json_list:
		if "concept_part_2" in j["question"] and j["answer"] !=0:
			j_answer.append({"heading": "{{" + "q39" + "}}" + "Насколько решение, которое Вы узнали из описания об этом новом продукте, подходит лично для Вас?<p>" + j["answer"] + "</p>"})
	return j_answer		

def createQ40(json_list):
	answer_json = []
	i = 1
	for j in json_list:
		if "concept_part_3" in j["question"] and j["answer"] !=0:
			answer_json.append({"text": j["answer"]})
			i+=1
	return answer_json	

def createQ34(json_list):
	answer_json = []
	for j in json_list:
		if "brand_name" in j["question"] and j["answer"] !=0:
			answer_json.append({"heading": "{{" + "q34" + "}}" + "Оцените, насколько Вам в целом нравится название марки?<p>" + j["answer"] + "</p>"})
	return answer_json	




def replaceAll ():
	"""Creates a survey template by replacing template's <> areas with data from fasdoc_data.json 
	and additional questions based on data
	"""
	data = json.load(open('fasdoc_data.json', encoding="utf8"))
	template = open('full_survey_template.json', encoding='utf-8')
	new_template = (template.read().replace("<brand_name>", str(createSurveyName(data)))
					.replace("<q10_categories>", str(createAnswer("category", data)))
					.replace("<q12_functions>", str(createAnswer("function", data)))
					.replace("<full_concept>", str(createHeading("full_concept", data)))
					.replace("<all_flavors_in_heading>", str(createHeading("flavor_name", data)))
					.replace("<all_sku_in_heading>", str(createHeading("sku", data)))
					.replace("<q15_concept_phrases>", str(createAnswer("phrase", data)))
					.replace("<all_flavors_with_composition>", str(createHeading("flavor", data)))
					.replace("<q16_flavor>", str(createAnswer("flavor_name", data)))
					.replace("<q24_flavors_with_neither>", str(createAnswerQ24("flavor_name", data)))
					.replace("<q20>", str(createSeveralQ20(data)))
					.replace("<q22_flavors>", str(createAnswer("flavor_name", data)))
					.replace("<q32>", str(createSeveralQ32(data)))
					.replace("<q33_functions>", str(createAnswer("function", data)))
					.replace("<q38_concept_part_1>", str(createQ38(data)))
					.replace("<q39_concept_part_2>", str(createQ39(data)))
					.replace("<q40_concept_part_3>", str(createQ40(data)))
					.replace("<q34_brand_name>", str(createQ34(data))))
	with open("Output.txt", "w") as text_file:
		text_file.write(new_template)

replaceAll ()














