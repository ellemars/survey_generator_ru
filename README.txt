Survey generator allows to create SurveyMonkey questionnaire template (in Russian) for Concept Testing (type of marketing research).

---------------
1) Place your fasdoc_data.json in root folder and launch script by "python create_survey_template.py"
2) The result will be an "Output.txt" with questionnaire template of json structure
3) Send this template as a POST request to SurveyMonkey API (https://api.surveymonkey.com/v3/surveys) using your access token and get ready-to-launch survey in your system display.